
# pytCal

pytCal est un projet domotique de gestion de chauffage.

Il commande les chauffages par envoie de messages mqtt pour domoticz.

| modules      |               |
| ------------ | ------------- |
| pytcal_core  | projet public | 
| pytcal_doc   | projet public |
| pytcal_rest  | projet privé  |
| pytcal_vuejs | projet privé  |


# pytcal_core



```plantuml
@startuml

<style>
header {
    HorizontalAlignment center
}
</style>
skinparam header{
    FontSize 20
}
skinparam legend {
  BackgroundColor #ffffffaa
  FontSize 12
}
skinparam backgroundcolor #ffffff11
skinparam ArrowThickness 4
skinparam ArrowColor Salmon
skinparam actorStyle awesome
skinparam roundCorner 10
skinparam Shadowing false
skinparam componentStyle uml2
skinparam registrars {

}
skinparam rectangle {
    roundCorner 0
    Shadowing false
    BackgroundColor TECHNOLOGY
    BorderColor Grey
}
skinparam component {
    Shadowing false
    BackgroundColor Chartreuse
    BorderColor Black
}
skinparam card {
    BackgroundColor White
}

skinparam ActorFontColor LightSkyBlue
skinparam ActorFontStyle bold
skinparam actor {
    BackgroundColor LightSkyBlue
    BorderColor MediumBlue
}
left to right direction
'top to bottom direction

'!include pytcal_core.iuml
'!include footer.iuml

header schéma simplifié \n

:admin:

card " " {
  rectangle   {
    [ pytcal.py ] as ccore
    file config.json as cconfig
  }
  agent crontab as ccrontab
  artifact "brocker mqtt" as cbrocker
  artifact "caldav" as ccaldav
  artifact "domotique" as cdomotique
}

collections chauffages  as cchauffages

ccrontab -> ccore #line:OrangeRed;text:OrangeRed :1
ccore -> cconfig #line:OrangeRed;text:OrangeRed :2 read
ccore --> ccaldav #line:OrangeRed;text:OrangeRed :3 read
ccore --> cbrocker #line:OrangeRed;text:OrangeRed :4
cdomotique -> cbrocker #line:OrangeRed;text:OrangeRed :5
cdomotique --> cchauffages #line:OrangeRed;text:OrangeRed :6

admin -> ccaldav #line:BlueViolet :config

legend right
  # ""cron"" exécute script
  # ""pytcal"" lit fichiers configuration
  # ""pytcal"" lit planification
  # ""pytcal"" envoie messages mqtt
  # ""domotique"" récupère messages
  # ""domotique"" commande chauffages
end legend

@enduml

```
