<!-- @format -->

# pytCal

pytCal est un projet domotique de gestion de chauffage.

Il commande les chauffages par envoie de messages mqtt pour domoticz.

| modules      |               |
| ------------ | ------------- |
| pytcal_core  | projet public | 
| pytcal_doc   | projet public |
| pytcal_rest  | projet privé  |
| pytcal_vuejs | projet privé  |

![schéma pytCal core](./pytcal_frontend.drawio.svg)

# pytcal_core

![schéma pytCal core](./plantuml/pytcal_core/pytcal_core.svg)

# config env

``` shell
sudo apt update -a
sudo apt install default-jre
sudo apt install graphviz
```
